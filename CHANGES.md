Since version 0.1:

  - No longer converts from UTF8 to LaTeX.

Version 0.1 (2018-05-XX)

  - Initial release, basic functionality
